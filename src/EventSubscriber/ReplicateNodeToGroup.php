<?php

namespace Drupal\replicate_actions\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\group\Entity\GroupRelationship;
use Drupal\node\NodeInterface;
use Drupal\replicate\Events\AfterSaveEvent;
use Drupal\replicate\Events\ReplicateAlterEvent;
use Drupal\replicate\Events\ReplicatorEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Makes replicated nodes added to group if required.
 */
class ReplicateNodeToGroup implements EventSubscriberInterface {

  /**
   * Current node ID.
   *
   * @var string
   */
  protected string $nid;

  /**
   * ReplicateNodeToGroup constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(protected ModuleHandlerInterface $moduleHandler, protected EntityTypeManagerInterface $entityTypeManager) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[ReplicatorEvents::REPLICATE_ALTER][] = ['saveClonedEntityId', 2];
    $events[ReplicatorEvents::AFTER_SAVE][] = ['addToGroups', 2];

    return $events;
  }

  /**
   * Store cloned node ID.
   *
   * @param \Drupal\replicate\Events\ReplicateAlterEvent $event
   *   The event fired by the replicator.
   */
  public function saveClonedEntityId(ReplicateAlterEvent $event): void {
    $clonedEntity = $event->getEntity();

    if (!$clonedEntity instanceof NodeInterface) {
      return;
    }

    $this->nid = $event->getOriginal()->id();
  }

  /**
   * Add node to Groups if original node was in groups.
   *
   * @param \Drupal\replicate\Events\AfterSaveEvent $event
   *   The event fired by the replicator.
   */
  public function addToGroups(AfterSaveEvent $event): void {
    $clonedEntity = $event->getEntity();

    if (!$clonedEntity instanceof NodeInterface
      || !$this->moduleHandler->moduleExists('group')
    ) {
      return;
    }

    // Find all groups where a node may have been added before.
    if ($this->nid) {
      $originalNode = $this->entityTypeManager
        ->getStorage('node')
        ->load($this->nid);
      $groupContents = GroupRelationship::loadByEntity($originalNode);
      foreach ($groupContents as $groupRelationship) {
        if ($groupRelationship->getEntity()->getEntityTypeId() == 'node') {
          $group = $groupRelationship->getGroup();
          $pluginId = 'group_node:' . $clonedEntity->bundle();
          $group->addRelationship($clonedEntity, $pluginId);
        }
      }
    }
  }

}
