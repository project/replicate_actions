<?php

namespace Drupal\replicate_actions\EventSubscriber;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\Url;
use Drupal\replicate\Events\AfterSaveEvent;
use Drupal\replicate\Events\ReplicateAlterEvent;
use Drupal\replicate\Events\ReplicatorEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Makes replicated entities unpublished and redirect to edit mode.
 */
class ReplicateSetEntityEdit implements EventSubscriberInterface {

  /**
   * ReplicateSetEntityEdit constructor.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $timeService
   *   Injected time object.
   */
  public function __construct(protected TimeInterface $timeService) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[ReplicatorEvents::REPLICATE_ALTER][] = ['setUnpublished', 1];
    $events[ReplicatorEvents::AFTER_SAVE][] = ['makeRedirect', 1];

    return $events;
  }

  /**
   * Sets the status of a replicated node to unpublished.
   *
   * @param \Drupal\replicate\Events\ReplicateAlterEvent $event
   *   The event fired by the replicator.
   */
  public function setUnpublished(ReplicateAlterEvent $event): void {
    $clonedEntity = $event->getEntity();

    // Don't allow the paragraph entities to be unpublished.
    if ($clonedEntity->getEntityTypeId() == 'paragraph') {
      return;
    }

    // Don't allow non-reusable block content entities (from Layout Builder)
    // to be unpublished.
    if ($clonedEntity->getEntityTypeId() === 'block_content' && !$clonedEntity->isReusable()) {
      return;
    }

    if ($this->isPublishableType($clonedEntity)) {

      // We don't need to check if it's instanceof TranslatableInterface since
      // at this point it's guaranteed to be a content entity (which implement
      // TranslatableInterface). If the above check changes in the future,
      // we would need to make sure this is translatable before getting
      // languages.
      foreach ($clonedEntity->getTranslationLanguages() as $translation_language) {
        /**
         * @var \Drupal\Core\Entity\ContentEntityTypeInterface|\Drupal\Core\Entity\EntityPublishedInterface $translation
         */
        $translation = $clonedEntity->getTranslation($translation_language->getId());
        $translation->setUnpublished();

        // Set the correct created/changed dates.
        //
        // There is currently no interface or other object-oriented indicator
        // that an entity has the concept of created/changed times.
        // See https://www.drupal.org/project/drupal/issues/2833378.
        $current_time = $this->timeService->getCurrentTime();
        if (method_exists($clonedEntity, 'setCreatedTime')) {
          $translation->setCreatedTime($current_time);
        }
        if (method_exists($clonedEntity, 'setChangedTime')) {
          $translation->setChangedTime($current_time);
        }

      }
    }
  }

  /**
   * Make a redirect to "edit" mode.
   *
   * @param \Drupal\replicate\Events\AfterSaveEvent $event
   *   The event fired by the replicator.
   *
   * @return void
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function makeRedirect(AfterSaveEvent $event): void {
    /** @var \Drupal\Core\Entity\EntityInterface $clonedEntity */
    $clonedEntity = $event->getEntity();
    $entityType = $clonedEntity->getEntityTypeId();

    try {
      if ($this->isPublishableType($clonedEntity)) {
        $editFormUrl = $clonedEntity->toUrl('edit-form');
        $edit_form_url = Url::fromRoute($editFormUrl->getRouteName(), [
          $entityType => $clonedEntity->id(),
        ]);
        $response = new RedirectResponse($edit_form_url->toString());
        $response->send();
      }
    }
    catch (UndefinedLinkTemplateException $e) {
      // Do nothing for this exception, it's acceptable to throw it away.
    }
  }

  /**
   * Helper method to report if the provided entity supports published status.
   *
   * @param \Drupal\Core\Entity\EntityInterface $clonedEntity
   *   The cloned entity to check.
   *
   * @return bool
   *   TRUE if published status is supported, FALSE otherwise.
   */
  private function isPublishableType(EntityInterface $clonedEntity) : bool {
    $publishable = FALSE;
    if ($clonedEntity instanceof ContentEntityInterface && $clonedEntity instanceof EntityPublishedInterface) {
      $publishable = TRUE;
    }
    return $publishable;
  }

}
