# Replicate Actions

[![Stand With Ukraine](https://raw.githubusercontent.com/vshymanskyy/StandWithUkraine/main/banner-direct-single.svg)](https://stand-with-ukraine.pp.ua)

While Replicating Nodes using
[Replicate API](https://www.drupal.org/project/replicate) and
[Replicate UI](https://www.drupal.org/project/replicate_ui) modules,
I found that the module will publish a node immediately (shows "view" mode)
after replication.
What I would like is when the replicated node opens in edit mode so that
a content manager can make the necessary changes before publishing it.
Otherwise, it creates too many steps for them to go through.

For now, the module makes replicated entities
unpublished and makes redirect to edit mode.

Also, if the [Group](https://www.drupal.org/project/group) module is installed,
this module checks whether a node has been added to a group
and does exactly that.
But in the future, incoming versions, I am going to add new functionality
and allow doing the same for other entity types and some more actions.

This provides the extension for a simple but powerful
[Replicate UI](https://www.drupal.org/project/replicate_ui).


## More information
- To issue any bug reports, feature or support requests, see the module issue
  queue at https://www.drupal.org/project/issues/2922694.

Author: Ruslan Piskarov <http://drupal.org/user/424444>.
